package com.example.nguyen_anh_tuan_test.repositories;

import com.example.nguyen_anh_tuan_test.models.entity.CategoryTag;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoryTagReponsetory extends JpaRepository<CategoryTag, Integer> {

    CategoryTag findCategoryTagByTag(String tag);
}
