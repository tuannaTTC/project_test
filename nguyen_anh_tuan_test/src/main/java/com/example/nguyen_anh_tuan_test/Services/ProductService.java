package com.example.nguyen_anh_tuan_test.Services;

import com.example.nguyen_anh_tuan_test.models.entity.CategoryTag;
import com.example.nguyen_anh_tuan_test.models.entity.Product;
import com.example.nguyen_anh_tuan_test.models.request.CreateProductRequest;
import com.example.nguyen_anh_tuan_test.models.response.BaseResponse;
import com.example.nguyen_anh_tuan_test.models.response.ProductFilterResponse;
import com.example.nguyen_anh_tuan_test.repositories.CategoryTagReponsetory;
import com.example.nguyen_anh_tuan_test.repositories.ProductReponsetory;
import com.example.nguyen_anh_tuan_test.utils.StatusResponse;
import com.example.nguyen_anh_tuan_test.utils.StringResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class ProductService {

    @Autowired
    private ProductReponsetory reponsetory;

    @Autowired
    private CategoryTagReponsetory categoryTagReponsetory;

    public ResponseEntity<?> read() {
        List<Product> products = reponsetory.findAll();
        return ResponseEntity.ok(new BaseResponse<>(StatusResponse.OK, StringResponse.SUCCESS, products));
    }

    public ResponseEntity<?> readByCategoryName(String categoryName) {

        List<List<?>> filter = reponsetory.filterByCategoryName(categoryName);

        List<ProductFilterResponse> list = filter.stream()
                                                .map(it -> {
                                                    return new ProductFilterResponse(it.get(0).toString(), it.get(1).toString(),it.get(2).toString(),Double.parseDouble(it.get(3).toString()));
                                                })
                                                .collect(Collectors.toList());
        this.sapXep(list);

        return ResponseEntity.ok(new BaseResponse<>(200, StringResponse.SUCCESS, list));
    }

    private void sapXep(List<ProductFilterResponse> list) {
        Collections.sort(list, new Comparator<ProductFilterResponse>() {
            @Override
            public int compare(ProductFilterResponse object1, ProductFilterResponse object2) {
                int rs = object1.getPrice().compareTo(object2.getPrice());
                if (rs == 0) {
                    return  object1.getName().compareTo(object2.getName());
                } else {
                    return rs;
                }
            }
        });
    }

    public ResponseEntity<?> create(CreateProductRequest request) {

        CategoryTag tag = categoryTagReponsetory.findCategoryTagByTag(request.getCategoryTag());

        if (tag == null) {
            return ResponseEntity.ok(new BaseResponse<>(StatusResponse.BAD_REQUEST, "Tag does not exist", request));
        }

        Product newProduct = new Product();
        newProduct.setName(request.getName());
        newProduct.setPrice(request.getPrice());
        newProduct.setCategoryTag(request.getCategoryTag());

        reponsetory.save(newProduct);

        return ResponseEntity.ok(new BaseResponse<>(StatusResponse.OK, StringResponse.SUCCESS, newProduct));
    }

    public ResponseEntity<?> update(Product request) {

        Product product = reponsetory.findById(request.getId()).orElse(null);

        if (product == null) {
            return ResponseEntity.ok(new BaseResponse<>(StatusResponse.BAD_REQUEST, "Product does not exist", request));
        }

        product = reponsetory.save(request);
        return ResponseEntity.ok(new BaseResponse<>(StatusResponse.OK, StringResponse.SUCCESS, product));
    }

    public ResponseEntity<?> delete(Integer id) {
        Product product = reponsetory.findById(id).orElse(null);

        if (product == null) {
            return ResponseEntity.ok(new BaseResponse<>(StatusResponse.BAD_REQUEST, "Product does not exist", id));
        }

        reponsetory.delete(product);
        return ResponseEntity.ok(new BaseResponse<>(StatusResponse.OK, StringResponse.SUCCESS, product));
    }
}
