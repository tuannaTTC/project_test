package com.example.nguyen_anh_tuan_test.Services;

import com.example.nguyen_anh_tuan_test.models.entity.CategoryTag;
import com.example.nguyen_anh_tuan_test.models.request.CreateCateloryTagRequest;
import com.example.nguyen_anh_tuan_test.models.response.BaseResponse;
import com.example.nguyen_anh_tuan_test.repositories.CategoryTagReponsetory;
import com.example.nguyen_anh_tuan_test.utils.StatusResponse;
import com.example.nguyen_anh_tuan_test.utils.StringResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryTagService {

    @Autowired
    private CategoryTagReponsetory reponsetory;

    public ResponseEntity<?> read() {

        List<CategoryTag> categoryTags = reponsetory.findAll();
        return ResponseEntity.ok(new BaseResponse<>(StatusResponse.OK, StringResponse.SUCCESS, categoryTags));
    }

    public ResponseEntity<?> create(CreateCateloryTagRequest request) {

        CategoryTag categoryTag = reponsetory.findCategoryTagByTag(request.getTag());

        if (categoryTag != null) {
            return ResponseEntity.ok(new BaseResponse<>(StatusResponse.BAD_REQUEST, "Category does exist", categoryTag));
        }

        CategoryTag newCategoryTag = new CategoryTag();
        newCategoryTag.setName(request.getName());
        newCategoryTag.setTag(request.getTag());

        reponsetory.save(newCategoryTag);

        return ResponseEntity.ok(new BaseResponse<>(StatusResponse.OK, StringResponse.SUCCESS, newCategoryTag));
    }

    public ResponseEntity<?> update(CategoryTag request) {

        CategoryTag categoryTag = reponsetory.findById(request.getId()).orElse(null);

        if (categoryTag == null) {
            return ResponseEntity.ok(new BaseResponse<>(StatusResponse.BAD_REQUEST, "Category does not exist", categoryTag));
        }

        categoryTag = reponsetory.save(request);

        return ResponseEntity.ok(new BaseResponse<>(StatusResponse.OK, StringResponse.SUCCESS, categoryTag));
    }

    public ResponseEntity<?> delete(Integer id) {
        CategoryTag categoryTag = reponsetory.findById(id).orElse(null);

        if (categoryTag == null) {
            return ResponseEntity.ok(new BaseResponse<>(StatusResponse.BAD_REQUEST, "Category does not exist", categoryTag));
        }
        reponsetory.delete(categoryTag);
        return ResponseEntity.ok(new BaseResponse<>(StatusResponse.OK, StringResponse.SUCCESS, categoryTag));
    }
}
