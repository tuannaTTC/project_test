package com.example.nguyen_anh_tuan_test.models.response;

public class ProductFilterResponse {

    private String name;

    private String categoryTag;

    private String categoryName;

    private Double price;

    public ProductFilterResponse() {
    }

    public ProductFilterResponse(String name, String categoryTag, String categoryName, Double price) {
        this.name = name;
        this.categoryTag = categoryTag;
        this.categoryName = categoryName;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategoryTag() {
        return categoryTag;
    }

    public void setCategoryTag(String categoryTag) {
        this.categoryTag = categoryTag;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }
}
