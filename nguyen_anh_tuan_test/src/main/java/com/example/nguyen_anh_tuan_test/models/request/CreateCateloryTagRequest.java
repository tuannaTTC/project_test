package com.example.nguyen_anh_tuan_test.models.request;

import javax.validation.constraints.NotBlank;

public class CreateCateloryTagRequest {

    @NotBlank
    private String name;

    @NotBlank
    private String tag;

    public CreateCateloryTagRequest() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }
}
