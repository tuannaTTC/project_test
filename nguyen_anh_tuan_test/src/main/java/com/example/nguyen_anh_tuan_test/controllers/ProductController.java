package com.example.nguyen_anh_tuan_test.controllers;

import com.example.nguyen_anh_tuan_test.Services.ProductService;
import com.example.nguyen_anh_tuan_test.models.entity.Product;
import com.example.nguyen_anh_tuan_test.models.request.CreateProductRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Min;

@RestController
@RequestMapping("/products")
@Validated
public class ProductController {

    @Autowired
    private ProductService service;

    @GetMapping()
    public ResponseEntity<?> read() {
        return service.read();
    }

    @GetMapping("/filter")
    public ResponseEntity<?> readByCategoryName(@RequestParam(name = "category_name") String categoryName) {
        return service.readByCategoryName(categoryName);
    }

    @PostMapping
    public ResponseEntity<?> create(@Valid @RequestBody CreateProductRequest request) {
        return service.create(request);
    }

    @PutMapping
    public ResponseEntity<?> update(@Valid @RequestBody Product request) {
        return service.update(request);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@Min(1) @PathVariable("id") Integer id) {
        return service.delete(id);
    }

}
