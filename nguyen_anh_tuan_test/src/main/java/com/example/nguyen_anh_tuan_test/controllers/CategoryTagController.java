package com.example.nguyen_anh_tuan_test.controllers;

import com.example.nguyen_anh_tuan_test.Services.CategoryTagService;
import com.example.nguyen_anh_tuan_test.models.entity.CategoryTag;
import com.example.nguyen_anh_tuan_test.models.request.CreateCateloryTagRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Min;

@RestController
@RequestMapping("/category-tags")
public class CategoryTagController {

    @Autowired
    private CategoryTagService service;

    @GetMapping()
    public ResponseEntity<?> read() {
        return service.read();
    }

    @PostMapping
    public ResponseEntity<?> create(@Valid @RequestBody CreateCateloryTagRequest request) {
        return service.create(request);
    }

    @PutMapping
    public ResponseEntity<?> update(@Valid @RequestBody CategoryTag request) {
        return service.update(request);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@Min(1) @PathVariable("id") Integer id) {
        return service.delete(id);
    }
}
